﻿using bus_backend.Services.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace bus_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusController : ControllerBase
    {
        private readonly IBusService _busService;

        public BusController(IBusService busService)
        {
            _busService = busService;
        }

        [HttpGet("GetBuses")]
        public async Task<IActionResult> GetBuses()
        {
            try
            {
                return Ok(await _busService.GetBuses());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("detail/{busId}")]
        public async Task<IActionResult> GetBusDetail(int busId)
        {
            try
            {
                return Ok(await _busService.GetBusDetail(busId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("detailForMap/{busId}")]
        public async Task<IActionResult> GetBusDetailForMap(int busId)
        {
            try
            {
                return Ok(await _busService.GetBusDetailForMap(busId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //[HttpGet("GetDirectionPolyLine/{directionId}")]
        //public async Task<IActionResult> GetDirectionPolyLine(int directionId)
        //{
        //    try
        //    {
        //        return Ok(await _busService.GetDirectionPolyline(directionId));
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }
        //}

        //[HttpGet("GetPlatformPoint/{platformId}")]
        //public async Task<IActionResult> GetPlatformPoint(int platformId)
        //{
        //    try
        //    {
        //        return Ok(await _busService.GetPlatformPoint(platformId));
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }
        //}
    }
}
