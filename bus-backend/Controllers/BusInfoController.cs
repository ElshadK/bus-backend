﻿using bus_backend.Core;
using bus_backend.Entities;
using bus_backend.Models;
using bus_backend.Services.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace bus_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusInfoController : ControllerBase
    {

        private readonly IActionService _actionService;

        public BusInfoController(IActionService actionService)
        {
            _actionService = actionService;
        }

        //[HttpGet]
        //public async Task<IActionResult> Get()
        //{
        //    try
        //    {
        //        return Ok();
        //        //using (StreamReader r = new StreamReader(@"C:\Users\ElshadK\Desktop\gis.json"))
        //        //{
        //        //    string json = r.ReadToEnd();
        //        //    List<DoublGisModel> items = JsonConvert.DeserializeObject<List<DoublGisModel>>(json);

        //        //    foreach (DoublGisModel item in items)
        //        //    {
        //        //        if (item.request.url.Contains("https://catalog.api.2gis.ru/3.0/items/byid?id=") && item.response.content.text!=null)
        //        //        {
        //        //           await _actionService.AddLink(new Link { url = item.request.url });

        //        //            BusInfoModel model = JsonConvert.DeserializeObject<BusInfoModel>(item.response.content.text);

        //        //            if (model.result != null && model.result.items != null)
        //        //            {
        //        //                for (int i = 0; i < model.result.items.Count; i++)
        //        //                {
        //        //                    var data = model.result.items[i];

        //        //                    Bus bus = new Bus
        //        //                    {
        //        //                        from_name = model.result.items[i].from_name,
        //        //                        to_name = model.result.items[i].to_name,
        //        //                        id = model.result.items[i].id,
        //        //                        locale = model.result.items[i].locale,
        //        //                        name = model.result.items[i].name,
        //        //                        region_id = model.result.items[i].region_id,
        //        //                        segment_id = model.result.items[i].segment_id,
        //        //                        subtype = model.result.items[i].subtype,
        //        //                        type = model.result.items[i].type,
        //        //                        isDeleted = false
        //        //                    };

        //        //                    int busId = await _actionService.AddBus(bus);

        //        //                    foreach (var dir in data.directions)
        //        //                    {
        //        //                        Geometry dirGeo = new Geometry
        //        //                        {
        //        //                            centroid = dir.geometry.centroid,
        //        //                            hover = dir.geometry.hover,
        //        //                            selection = dir.geometry.selection,
        //        //                            isDeleted = false
        //        //                        };

        //        //                        int geoDirId =await _actionService.AddGeometry(dirGeo);

        //        //                        Direction direction = new Direction
        //        //                        {
        //        //                            busId = busId,
        //        //                            geometryId = geoDirId,
        //        //                            id = dir.id,
        //        //                            isDeleted = false,
        //        //                            type = dir.type,
        //        //                        };

        //        //                        int dirId =await _actionService.AddDirection(direction);

        //        //                        foreach (var plat in dir.platforms)
        //        //                        {
        //        //                            int geoPlatId = 0;
        //        //                            if (!await _actionService.IsExsistPlatform(plat.station_id, plat.id))
        //        //                            {
        //        //                                Geometry platGeo = new Geometry
        //        //                                {
        //        //                                    centroid = plat.geometry.centroid,
        //        //                                    hover = plat.geometry.hover,
        //        //                                    selection = plat.geometry.selection,
        //        //                                    isDeleted = false
        //        //                                };
        //        //                                geoPlatId = await _actionService.AddGeometry(platGeo);
        //        //                            }

        //        //                            Platform platform = new Platform
        //        //                            {
        //        //                                geometryId = geoPlatId,
        //        //                                id = plat.id,
        //        //                                name = plat.name,
        //        //                                station_id = plat.station_id,
        //        //                                isDeleted = false,
        //        //                            };

        //        //                            int platId =await _actionService.AddPlatform(platform);

        //        //                            DirectionPlatform directionPlatform = new DirectionPlatform
        //        //                            {
        //        //                                directionId = dirId,
        //        //                                platformId = platId,
        //        //                                isDeleted = false,
        //        //                            };
        //        //                           await _actionService.AddDirectionPlatform(directionPlatform);
        //        //                        }
        //        //                    }
        //        //                }
        //        //            }
        //        //        }

        //        //    }
        //        //    return Ok(items);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        return Ok("Error: " + ex.Message);
        //    }
        //}
    }
}
