﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bus_backend.Models
{
    public class Order
    {
        public int busId { get; set; }
        public int num { get; set; }
        public int bus { get; set; }
    }
}
