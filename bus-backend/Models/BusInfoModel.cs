﻿using System.Collections.Generic;

namespace bus_backend.Models
{
    public class BusInfoModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);

        public Meta meta { get; set; }
        public Result result { get; set; }

        public class Meta
        {
            public string api_version { get; set; }
            public int code { get; set; }
            public string issue_date { get; set; }
        }

        public class Context
        {
        }

        public class Geometry
        {
            public string centroid { get; set; }
            public string selection { get; set; }
            public string hover { get; set; }
        }

        public class Platform
        {
            public Geometry geometry { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string place_name { get; set; }
            public string station_id { get; set; }
        }

        public class Direction
        {
            public Geometry geometry { get; set; }
            public string id { get; set; }
            public List<Platform> platforms { get; set; }
            public string type { get; set; }
        }

        public class Reviews
        {
            public bool is_reviewable { get; set; }
            public bool is_reviewable_on_flamp { get; set; }
        }

        public class Stat
        {
            public bool is_advertised { get; set; }
            public string source_type { get; set; }
        }

        public class Item
        {
      //      public Context context { get; set; }
            public List<Direction> directions { get; set; }
       //     public List<object> external_content { get; set; }
            public string from_name { get; set; }
            public string id { get; set; }
            public string locale { get; set; }
            public string name { get; set; }
            public string region_id { get; set; }
         //   public Reviews reviews { get; set; }
            public string segment_id { get; set; }
       //     public Stat stat { get; set; }
            public string subtype { get; set; }
            public string to_name { get; set; }
            public string type { get; set; }
        }

        public class Result
        {
            public List<Item> items { get; set; }
            public string total { get; set; }
        }
    }
}
