﻿using System;
using System.Collections.Generic;

namespace bus_backend.Models
{
    public class DoublGisModel
    {
        public Initiator _initiator { get; set; }
        public string _priority { get; set; }
        public string _resourceType { get; set; }
        public Cache cache { get; set; }
        public string connection { get; set; }
        public string pageref { get; set; }
        public Request request { get; set; }
        public Response response { get; set; }
        public string serverIPAddress { get; set; }
        public string startedDateTime { get; set; }
        public string time { get; set; }
        public Timings timings { get; set; }

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class CallFrame
        {
            public string functionName { get; set; }
            public string scriptId { get; set; }
            public string url { get; set; }
            public string lineNumber { get; set; }
            public string columnNumber { get; set; }
        }

        public class Parent
        {
            public string description { get; set; }
            public List<CallFrame> callFrames { get; set; }
            public Parent parent { get; set; }
        }

        public class Stack
        {
            public List<CallFrame> callFrames { get; set; }
            public Parent parent { get; set; }
        }

        public class Initiator
        {
            public string type { get; set; }
            public Stack stack { get; set; }
        }

        public class Cache
        {
        }

        public class Header
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class QueryString
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class Cooky
        {
            public string name { get; set; }
            public string value { get; set; }
            public string path { get; set; }
            public string domain { get; set; }
            public DateTime expires { get; set; }
            public bool httpOnly { get; set; }
            public bool secure { get; set; }
            public string sameSite { get; set; }
        }

        public class Request
        {
            public string method { get; set; }
            public string url { get; set; }
            public string httpVersion { get; set; }
            public List<Header> headers { get; set; }
            public List<QueryString> queryString { get; set; }
            public List<Cooky> cookies { get; set; }
            public string headersSize { get; set; }
            public string bodySize { get; set; }
        }

        public class Content
        {
            public string size { get; set; }
            public string mimeType { get; set; }
            public string text { get; set; }
            public string encoding { get; set; }
        }

        public class Response
        {
            public string status { get; set; }
            public string statusText { get; set; }
            public string httpVersion { get; set; }
            public List<Header> headers { get; set; }
            public List<object> cookies { get; set; }
            public Content content { get; set; }
            public string redirectURL { get; set; }
            public string headersSize { get; set; }
            public string bodySize { get; set; }
            public string _transferSize { get; set; }
            public object _error { get; set; }
        }

        public class Timings
        {
            public double blocked { get; set; }
            public string dns { get; set; }
            public string ssl { get; set; }
            public string connect { get; set; }
            public string send { get; set; }
            public string wait { get; set; }
            public string receive { get; set; }
            public string _blocked_queueing { get; set; }
        }
    }
}
