﻿namespace bus_backend.DTOs
{
    public class BusToList
    {
        public int busId { get; set; }
        public string from_name { get; set; }
        public string name { get; set; }
        public string to_name { get; set; }
    }
}
