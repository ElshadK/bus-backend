﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bus_backend.DTOs
{
    public class LatLongDTO
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
