﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bus_backend.DTOs
{
    public class BusDetailForMapDTO
    {
        public GeometryToList center { get; set; }
        public DirectionDTO backward { get; set; }
        public DirectionDTO forward { get; set; }
        public DirectionDTO circular { get; set; }
        public string type { get; set; }
    }
}
