﻿namespace bus_backend.DTOs
{
    public class GeometryToList
    {
        //public int geometryId { get; set; }
        //public string centroid { get; set; }
        //public string selection { get; set; }
        //  public string hover { get; set; }

        public double? latitude { get; set; }
        public double? longitude { get; set; }
    }
}
