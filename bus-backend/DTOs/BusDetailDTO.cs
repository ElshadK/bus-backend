﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bus_backend.DTOs
{
    public class BusDetailDTO
    {
        public BusToList bus { get; set; }
        public DirectionToList backward { get; set; }
        public DirectionToList forward { get; set; }
        public DirectionToList circular { get; set; }

     //   public List<DirectionToList> directions { get; set; }
    }
}
