﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bus_backend.DTOs
{
    public class DirectionDTO
    {
        public List<GeometryToList> polylines { get; set; }
        public List<GeometryToList> platforms { get; set; }
    }
}
