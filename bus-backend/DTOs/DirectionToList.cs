﻿using bus_backend.Entities;
using System.Collections.Generic;

namespace bus_backend.DTOs
{
    public class DirectionToList
    {
        public int directionId { get; set; }
    //    public GeometryToList geometry { get; set; }
        public List<PlatformToList> platforms { get; set; }
        public string type { get; set; }
    }
}
