﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace bus_backend.Entities
{
    public class Bus
    {
        [Key]
        public int busId { get; set; }
        public virtual List<Direction> directions { get; set; }
        public string from_name { get; set; }
        public string id { get; set; }
        public string locale { get; set; }
        public string name { get; set; }
        public int region_id { get; set; }
        public int segment_id { get; set; }
        public string subtype { get; set; }
        public string to_name { get; set; }
        public string type { get; set; }
        public int? rank { get; set; }
        public bool isDeleted { get; set; }

    }
}
