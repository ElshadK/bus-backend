﻿using System.ComponentModel.DataAnnotations;

namespace bus_backend.Entities
{
    public class Link
    {
        [Key]
        public int linkId { get; set; }
        public string url { get; set; }
        public bool isDeleted { get; set; }
    }
}
