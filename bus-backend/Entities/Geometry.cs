﻿using System.ComponentModel.DataAnnotations;

namespace bus_backend.Entities
{
    public class Geometry
    {
        [Key]
        public int geometryId { get; set; }
      //  public string centroid { get; set; }
        public string selection { get; set; }
      //  public string hover { get; set; }
        public double? latitude { get; set; }
        public double? longitude { get; set; }

        public bool isDeleted { get; set; }
    }
}
