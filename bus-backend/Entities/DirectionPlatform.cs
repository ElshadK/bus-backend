﻿using System.ComponentModel.DataAnnotations;

namespace bus_backend.Entities
{
    public class DirectionPlatform
    {
        [Key]
        public int directionPlatformId { get; set; }
        public int platformId { get; set; }
        public int directionId { get; set; }
        public bool isDeleted { get; set; }

    }
}
