﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace bus_backend.Entities
{
    public class Direction
    {
        [Key]
        public int directionId { get; set; }
        public int busId { get; set; }
        public int geometryId { get; set; }
        public virtual Geometry geometry { get; set; }
        //     public string id { get; set; }
        // public virtual List<Platform> platforms { get; set; }
        public string type { get; set; }
        public bool isDeleted { get; set; }

    }
}
