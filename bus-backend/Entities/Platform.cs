﻿using System.ComponentModel.DataAnnotations;

namespace bus_backend.Entities
{
    public class Platform
    {
        [Key]
        public int platformId { get; set; }
        public int geometryId { get; set; }
        public virtual Geometry geometry { get; set; }
   //     public string id { get; set; }
        public string name { get; set; }
   //     public string station_id { get; set; }
        public bool isDeleted { get; set; }

    }
}
