﻿using bus_backend.DAL;
using bus_backend.Entities;
using bus_backend.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace bus_backend.Services
{
    public class ActionService : IActionService
    {
        private readonly DataContext _dataContext;
        public ActionService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<int> AddBus(Bus bus)
        {
            var result = await _dataContext.Buses.AddAsync(bus);
            await _dataContext.SaveChangesAsync();
            return result.Entity.busId;
        }

        public async Task<int> AddDirection(Direction direction)
        {
            var result = await _dataContext.Directions.AddAsync(direction);
            await _dataContext.SaveChangesAsync();
            return result.Entity.directionId;
        }

        public async Task<int> AddDirectionPlatform(DirectionPlatform directionPlatform)
        {
            var result = await _dataContext.DirectionPlatforms.AddAsync(directionPlatform);
            await _dataContext.SaveChangesAsync();
            return result.Entity.directionPlatformId;
        }

        public async Task<int> AddGeometry(Geometry geometry)
        {
            var result = await _dataContext.Geometries.AddAsync(geometry);
            await _dataContext.SaveChangesAsync();
            return result.Entity.geometryId;
        }

        public async Task<int> AddLink(Link link)
        {
            var result = await _dataContext.Links.AddAsync(link);
            await _dataContext.SaveChangesAsync();
            return result.Entity.linkId;
        }

        public async Task<int> AddPlatform(Platform platform)
        {
            return 0;
            //Platform plat = await _dataContext.Platforms.FirstOrDefaultAsync(x => x.station_id == platform.station_id && x.id == platform.id);
            //if (plat != null)
            //{
            //    return plat.platformId;
            //}
            //else
            //{
            //    var result = await _dataContext.Platforms.AddAsync(platform);
            //    await _dataContext.SaveChangesAsync();
            //    return result.Entity.platformId;
            //}
        }

        public async Task<bool> IsExsistPlatform(string station_id, string id)
        {
            return false; //  return await _dataContext.Platforms.AnyAsync(x => x.station_id == station_id && x.id == id);
        }
    }
}
