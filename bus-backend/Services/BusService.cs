﻿using AutoMapper;
using bus_backend.DAL;
using bus_backend.DTOs;
using bus_backend.Entities;
using bus_backend.Models;
using bus_backend.Services.IServices;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bus_backend.Services
{

    public class BusService : IBusService
    {
        private readonly IMapper _mapper;
        private readonly DataContext _dataContext;
        public BusService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<BusDetailDTO> GetBusDetail(int busId)
        {
            BusDetailDTO busDetail = new BusDetailDTO();

            busDetail.bus = _mapper.Map<BusToList>(await _dataContext.Buses.FirstOrDefaultAsync(x => x.busId == busId));

            List<Direction> directions = await _dataContext.Directions.Where(x => x.busId == busId).ToListAsync();
            List<DirectionToList> directionToLists = _mapper.Map<List<DirectionToList>>(directions);

            foreach (DirectionToList direction in directionToLists)
            {
                List<int> platIds = await _dataContext.DirectionPlatforms.Where(x => x.directionId == direction.directionId).Select(x => x.platformId).ToListAsync();

                List<Platform> platforms = await _dataContext.Platforms.Where(x => platIds.Contains(x.platformId)).ToListAsync();
                direction.platforms = _mapper.Map<List<PlatformToList>>(platforms);
            }

            busDetail.backward = directionToLists.FirstOrDefault(x => x.type == "backward");
            busDetail.forward = directionToLists.FirstOrDefault(x => x.type == "forward");
            busDetail.circular = directionToLists.FirstOrDefault(x => x.type == "circular");

            return busDetail;
        }

        public async Task<BusDetailForMapDTO> GetBusDetailForMap(int busId)
        {

            BusDetailForMapDTO busDetailForMap = new BusDetailForMapDTO();
            busDetailForMap.forward = new DirectionDTO();
            busDetailForMap.backward = new DirectionDTO();
            busDetailForMap.circular = new DirectionDTO();

            List<Direction> directions = await _dataContext.Directions.Where(x => x.busId == busId).OrderBy(x => x.directionId).ToListAsync();

            foreach (Direction direction in directions)
            {
                List<int> platIds = await _dataContext.DirectionPlatforms.Where(x => x.directionId == direction.directionId).Select(x => x.platformId).ToListAsync();

                List<Geometry> platGeos = await _dataContext.Platforms.Where(x => platIds.Contains(x.platformId)).Select(x => x.geometry).ToListAsync();
                busDetailForMap.center = new GeometryToList { latitude = direction.geometry.latitude, longitude = direction.geometry.longitude };
                busDetailForMap.type = direction.type;

                if (direction.type == "backward")
                {
                    busDetailForMap.backward.polylines = JsonConvert.DeserializeObject<List<GeometryToList>>(direction.geometry.selection);
                    busDetailForMap.backward.platforms = _mapper.Map<List<GeometryToList>>(platGeos);
                }
                else if (direction.type == "forward")
                {
                    busDetailForMap.forward.polylines = JsonConvert.DeserializeObject<List<GeometryToList>>(direction?.geometry?.selection);
                    busDetailForMap.forward.platforms = _mapper.Map<List<GeometryToList>>(platGeos);
                }
                else if (direction.type == "circular")
                {
                    busDetailForMap.circular.polylines = JsonConvert.DeserializeObject<List<GeometryToList>>(direction?.geometry?.selection);
                    busDetailForMap.circular.platforms = _mapper.Map<List<GeometryToList>>(platGeos);
                }
            }
            return busDetailForMap;
        }

        public async Task<List<BusToList>> GetBuses()
        {
            List<Bus> buses = await _dataContext.Buses.OrderBy(x => x.name).OrderBy(x=>x.rank).ToListAsync();

            return _mapper.Map<List<BusToList>>(buses);
        }



        public async Task Help()
        {
            //List<Geometry> geos = await _dataContext.Geometries.Where(x => x.selection.Contains("LINESTRING")).ToListAsync();

            //foreach (Geometry geo in geos)
            //{
            //    List<LatLongDTO> latLongs = new List<LatLongDTO>();

            //    string ff = geo.selection.Trim();
            //    string item = ff.Substring(11, ff.Length - 12);
            //    string[] coor = item.Split(',');

            //    foreach (string val in coor)
            //    {
            //        string[] d = val.Trim().Split(' ');
            //        LatLongDTO latLong = new LatLongDTO { latitude = double.Parse(d[1]), longitude = double.Parse(d[0]) };
            //        latLongs.Add(latLong);
            //    }
            //    geo.selection = JsonConvert.SerializeObject(latLongs);
            //}
            //await _dataContext.SaveChangesAsync();


            //string geo = direction.geometry.selection.Trim();
            //string item = geo.Substring(11, geo.Length - 12);
            //string[] coor = item.Split(',');

            //foreach (string val in coor)
            //{
            //    string[] d = val.Trim().Split(' ');
            //    LatLongDTO latLong = new LatLongDTO { latitude = double.Parse(d[1]), longitude = double.Parse(d[0]) };
            //    latLongs.Add(latLong);
            //}


            ///  List<LatLongDTO> latLongs = new List<LatLongDTO>();
            //   Direction direction = await _dataContext.Directions.FirstOrDefaultAsync(x => x.directionId == directionId);

            //string geo = direction.geometry.selection.Trim();
            //string item = geo.Substring(11, geo.Length - 12);
            //string[] coor = item.Split(',');

            //foreach (string val in coor)
            //{
            //    string[] d = val.Trim().Split(' ');
            //    LatLongDTO latLong = new LatLongDTO { latitude = double.Parse(d[1]), longitude = double.Parse(d[0]) };
            //    latLongs.Add(latLong);
            //}

            //  return latLongs;

            //List<Geometry> geos = await _dataContext.Geometries.Where(x => x.selection.Contains("POINT")).ToListAsync();

            //foreach (Geometry geo in geos)
            //{
            //    var item = JsonConvert.DeserializeObject<LatLongDTO>(geo.selection);
            //    geo.latitude = item.latitude;
            //    geo.longitude = item.longitude;

            //    //string item = geo.selection.Trim();
            //    //string[] d = item.Substring(6, item.Length - 7).Split(' ');
            //    //LatLongDTO latLong = new LatLongDTO { latitude = double.Parse(d[1]), longitude = double.Parse(d[0]) };
            //    //geo.selection = JsonConvert.SerializeObject(latLong);
            //}
            //await _dataContext.SaveChangesAsync();



            //List<Direction> directions = await _dataContext.Directions.Where(x => x.busId == busId).ToListAsync();

            //BusToList bus = _mapper.Map<BusToList>(await _dataContext.Buses.FirstOrDefaultAsync(x => x.busId == busId));

            //List<DirectionToList> directionToLists = _mapper.Map<List<DirectionToList>>(directions);

            //foreach (DirectionToList direction in directionToLists)
            //{
            //    List<int> platIds = await _dataContext.DirectionPlatforms.Where(x => x.directionId == direction.directionId).Select(x => x.platformId).ToListAsync();

            //    List<Platform> platforms = await _dataContext.Platforms.Where(x => platIds.Contains(x.platformId)).ToListAsync();
            //    direction.platforms = _mapper.Map<List<PlatformToList>>(platforms);

            //foreach (var item in direction.platforms)
            //{
            //    item.coordinate = JsonConvert.DeserializeObject<LatLongDTO>(item.geometry.selection);
            //}

            //  }

            //    return null;
            // return new BusDetailDTO { bus = bus, directions = directionToLists };

            //List<Geometry> geos = await _dataContext.Geometries.Where(x => x.selection.Contains("latitude")).ToListAsync();

            //foreach (Geometry geo in geos)
            //{
            //    var item = JsonConvert.DeserializeObject<LatLongDTO>(geo.selection);
            //    geo.latitude = item.latitude;
            //    geo.longitude = item.longitude;

            //    //string item = geo.selection.Trim();
            //    //string[] d = item.Substring(6, item.Length - 7).Split(' ');
            //    //LatLongDTO latLong = new LatLongDTO { latitude = double.Parse(d[1]), longitude = double.Parse(d[0]) };
            //    //geo.selection = JsonConvert.SerializeObject(latLong);
            //}
            //await _dataContext.SaveChangesAsync();

            //  return null;
        }
    }
}
