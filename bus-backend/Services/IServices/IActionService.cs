﻿using bus_backend.Entities;
using System.Threading.Tasks;

namespace bus_backend.Services.IServices
{
    public interface IActionService
    {
         Task<int> AddGeometry(Geometry geometry);
        Task<int> AddDirection(Direction direction);
        Task<int> AddPlatform(Platform platform);
        Task<int> AddDirectionPlatform(DirectionPlatform directionPlatform);
        Task<int> AddBus(Bus bus);
        Task<int> AddLink(Link link);
        Task<bool> IsExsistPlatform(string station_id, string id);
    }
}
