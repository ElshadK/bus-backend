﻿using bus_backend.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace bus_backend.Services.IServices
{
    public interface IBusService
    {
        Task<List<BusToList>> GetBuses();
        Task<BusDetailForMapDTO> GetBusDetailForMap(int busId);
        Task<BusDetailDTO> GetBusDetail(int busId);
        Task Help();
    }
}
