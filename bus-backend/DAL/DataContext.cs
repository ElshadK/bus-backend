﻿using bus_backend.Entities;
using Microsoft.EntityFrameworkCore;

namespace bus_backend.DAL
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        public DbSet<Bus> Buses { get; set; }
        public DbSet<Direction> Directions { get; set; }
        public DbSet<Platform> Platforms { get; set; }
        public DbSet<Geometry> Geometries { get; set; }
        public DbSet<DirectionPlatform> DirectionPlatforms { get; set; }
        public DbSet<Link> Links { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bus>().HasQueryFilter(m => !m.isDeleted);
            modelBuilder.Entity<Direction>().HasQueryFilter(m => !m.isDeleted);
            modelBuilder.Entity<Platform>().HasQueryFilter(m => !m.isDeleted);
            modelBuilder.Entity<Geometry>().HasQueryFilter(m => !m.isDeleted);
            modelBuilder.Entity<DirectionPlatform>().HasQueryFilter(m => !m.isDeleted);
            modelBuilder.Entity<Link>().HasQueryFilter(m => !m.isDeleted);

        }
    }
}
