﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bus_backend.CustomMiddlewares
{
    public class ValidateTokenMiddleware
    {
        private readonly RequestDelegate _next;
        public ValidateTokenMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {

            string tokenString = httpContext.Request.Headers["Authorization"].ToString();
            if (tokenString!="busapp43rf90jwo23jewdpcmsdm3435aesfk")
            {
                httpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
                return;
            }
            await _next.Invoke(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ValidateTokenMiddlewareExtensions
    {
        public static IApplicationBuilder UseMiddlewareClassTemplate(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ValidateTokenMiddleware>();
        }
    }
}
