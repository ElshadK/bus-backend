﻿using AutoMapper;
using bus_backend.DTOs;
using bus_backend.Entities;

namespace bus_backend.Core
{
    public static class AutoMapperProfiles
    {
        public class AutoMapperProfile : Profile
        {
            public AutoMapperProfile()
            {
                CreateMap<Bus, BusToList>();
                CreateMap<Direction, DirectionToList>();
                CreateMap<Geometry, GeometryToList>();
                CreateMap<Platform, PlatformToList>();
            }
        }
    }
}
